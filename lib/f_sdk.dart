import 'dart:async';

import 'package:flutter/services.dart';

class FSdk {
  static const MethodChannel _channel = MethodChannel('f_sdk');
  int count = 0;
  bool flag = true;
  late Timer timer;
  List<String> screenShots = [];

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String?> sayHello(String screenName) async {
    final String? hello =
        await _channel.invokeMethod('sayHello', <String, dynamic>{'screenName': screenName});
    return hello;
  }

  static Future<String?> sayGoodbye(String screenName) async {
    final String? hello =
        await _channel.invokeMethod('sayGoodbye', <String, dynamic>{'screenName': screenName});
    return hello;
  }

  Future<String?> startRecording() async {
    print("startRecording");
    timer = Timer.periodic(
        const Duration(seconds: 1), (Timer t) => {_handleScreenTake()});

    print(count);
  }

  Future<String?> stopRecording() async {
    print("stopRecording $count Seconds");
    timer.cancel();
    count = 0;
  }

  _handleScreenTake() {
    count++;
    screenShots.add('screenshot-${DateTime.now()}.png');
    print("Count record $count Seconds");
  }
}
