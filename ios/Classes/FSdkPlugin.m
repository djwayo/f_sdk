#import "FSdkPlugin.h"
#if __has_include(<f_sdk/f_sdk-Swift.h>)
#import <f_sdk/f_sdk-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "f_sdk-Swift.h"
#endif

@implementation FSdkPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFSdkPlugin registerWithRegistrar:registrar];
}
@end
