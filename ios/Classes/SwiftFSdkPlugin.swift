import Flutter
import UIKit


public class SwiftFSdkPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "f_sdk", binaryMessenger: registrar.messenger())
        let instance = SwiftFSdkPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let args = call.arguments as? Dictionary<String, String>
        
        switch call.method {
        case "sayHello":
            let name = args?["screenName"]
            if name != nil  {
                result("Hello " +  name!)
            } else {
                result(FlutterError.init(code: "BAD_ARGS",
                                         message: "Wrong argument - need to set the screen name)" ,
                                         details: nil))
                
            }
        case "sayGoodbye":
            let name = args?["screenName"]
            if name != nil  {
                result("Goodbye " +  name!)
            } else {
                result(FlutterError.init(code: "BAD_ARGS",
                                         message: "Wrong argument - need to set the screen name)" ,
                                         details: nil))
                
            }
        default:
            result(FlutterMethodNotImplemented)
        }
    }
}
