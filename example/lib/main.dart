import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:f_sdk/f_sdk.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver{
  String _hello = 'Unknown';
  final String _screenName = "Home";
  late FSdk sdk;

  @override
  void initState() {
    super.initState();
    sdk = FSdk();
    sayHello();
    startSync();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
void dispose() {
  WidgetsBinding.instance!.removeObserver(this);
  super.dispose();
}

  void startSync() {
   sdk.startRecording();
  }

  void stopSync() {
    sdk.stopRecording();
  }

  Future<void> sayHello() async {
    String hello = "";

    try {
        hello = await FSdk.sayHello(_screenName) ??
            'Unknown platform hello';
      
    } on PlatformException  catch (err) {
      hello = err.message ?? "Not resolved sayHello";
    }

    if (!mounted) return;

    setState(() {
      _hello = hello;
    });
  }

  Future<void> sayGoodbye() async {
    String hello = "";
    try {
        hello = await FSdk.sayGoodbye(_screenName) ??
            'Unknown platform sayGoodbye';
      
    } on PlatformException  catch (err) {
      hello = err.message ?? "Not resolved sayGoodbye";
    }

    if (!mounted) return;

    setState(() {
      _hello = hello;
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
      startSync();
      sayHello();
        print("app in resumed");
        break;
      case AppLifecycleState.inactive:
        stopSync();
        sayGoodbye();
        print("app in inactive");
        break;
      case AppLifecycleState.paused:
        print("app in paused");
        break;
      case AppLifecycleState.detached:
        print("app in detached");
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text('Say: $_hello\n'),
        ),
      ),
    );
  }
}
